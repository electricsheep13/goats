package org.mcginn.goats.listing.db;

import java.util.List;

import org.mcginn.goats.core.Goat;

public interface GoatsListingDb {

	List<Goat> getAllGoats(int limit);
	List<Goat> getAllGoatsForOwner(String owner, int limit);
}
