package org.mcginn.goats.listing.db;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.apache.commons.lang.StringUtils;
import org.mcginn.goats.core.Goat;
import org.springframework.stereotype.Component;

@Component
public class MockGoatsListingDb implements GoatsListingDb {

	private final Map<String, List<Goat>> goatsByOwner;
	
	public MockGoatsListingDb() {
		super();
		this.goatsByOwner = new HashMap<>();
		
		List<Goat> myGoats = new ArrayList<Goat>();
		myGoats.add(new Goat("Ellie", "McWoukinn Farms", "Nubian", LocalDate.of(2014, 4, 11)));
		myGoats.add(new Goat("Willow", "McWoukinn Farms", "Nubian", LocalDate.of(2014, 5, 4)));
		myGoats.add(new Goat("Thor", "McWoukinn Farms", "Nubian", LocalDate.of(2015, 6, 4)));
		myGoats.add(new Goat("Loki", "McWoukinn Farms", "Nubian", LocalDate.of(2015, 6, 4)));
		this.goatsByOwner.put("McWoukinn Farms", myGoats);
		
		List<Goat> myLeasedGoats = new ArrayList<Goat>();
		myLeasedGoats.add(new Goat("Fleetwood Mac", "Frogflat Farms", "Nubian", LocalDate.of(2015, 1, 15)));
		this.goatsByOwner.put("Frogflat Farms", myLeasedGoats);
	}

	@Override
	public List<Goat> getAllGoats(int limit) {
		return filterAndSortGoats(goat -> true, limit);
	}
	
	@Override
	public List<Goat> getAllGoatsForOwner(String owner, int limit) {
		return filterAndSortGoats(goat -> StringUtils.equals(owner, goat.getOwner()), limit);
	}

	private List<Goat> filterAndSortGoats(Predicate<Goat> filter, int limit) {

		List<Goat> results = new ArrayList<>(limit);
		this.goatsByOwner.values().forEach(list -> {
			if(results.size() < limit) {
				list.stream().filter(filter).forEach(goat-> {
					if(results.size() < limit) {
						results.add(goat);
					}
				});
			}
		});
		return results;
	}
}
