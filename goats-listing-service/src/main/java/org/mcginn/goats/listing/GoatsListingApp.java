package org.mcginn.goats.listing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoatsListingApp {

	public static void main(String[] args) {
		SpringApplication.run(GoatsListingApp.class, args);
	}
}
