package org.mcginn.goats.listing;

import java.util.List;

import org.mcginn.goats.core.Goat;
import org.mcginn.goats.listing.db.GoatsListingDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class GoatsListingService {

	public static final int DEFAULT_GOATS_LISTING_LIMIT = 40;
	
	@Autowired
	private GoatsListingDb goatsListingDb;
	
	@RequestMapping(method=RequestMethod.GET, value="/goats", produces="application/json")
	public List<Goat> getAllGoats(@RequestParam(name = "limit", required = false) Integer limit) {
		if (limit == null) {
			limit = DEFAULT_GOATS_LISTING_LIMIT;
		}
		return this.goatsListingDb.getAllGoats(limit);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/owners/{owner}/goats", produces="application/json")
	public List<Goat> getGoatsForOwner(@PathVariable("owner") String owner, @RequestParam(name = "limit", required = false) Integer limit) {
        if (limit == null) {
            limit = DEFAULT_GOATS_LISTING_LIMIT;
        }
		return this.goatsListingDb.getAllGoatsForOwner(owner, limit);
	}
}
