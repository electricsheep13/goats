package org.mcginn.goats.core;

import java.io.Serializable;
import java.time.LocalDate;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Goat implements Serializable {

	private static final long serialVersionUID = 5212949327791583393L;
	
	private final String name;
	private final String owner;
	private final String breed;
	private final LocalDate birthday;
	
	public Goat(String name, String owner, String breed, LocalDate birthday) {
		super();
		this.name = name;
		this.owner = owner;
		this.breed = breed;
		this.birthday = birthday;
	}

	public String getName() {
		return name;
	}

	public String getOwner() {
		return owner;
	}

	public String getBreed() {
		return breed;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.getOwner())
				.append(this.getName())
				.append(this.getBirthday()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this)  {
			return true;
		}
		
		if(obj == null || !(obj instanceof Goat)) {
			return false;
		}
		
		Goat other = (Goat)obj;
		
		return new EqualsBuilder().append(this.getOwner(), other.getOwner())
				.append(this.getName(), other.getName())
				.append(this.getBirthday(), other.getBirthday()).isEquals();
	}

	@Override
	public String toString() {
		return String.format("%s goat %s owned by %s born on %tD", this.getBreed(), this.getName(), this.getOwner(), this.getBirthday());
	}
}
