package org.mcginn.accounts.db;

import static org.junit.Assert.assertNotNull;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Before;
import org.junit.Test;
import org.mcginn.accounts.core.Account;
import org.mcginn.accounts.login.hash.MessageDigestPasswordHash;
import org.mcginn.accounts.login.hash.PasswordHash;

public class MockAccountsDbTest {

	private MockAccountsDb db;
	private PasswordHash hasher;
	
	@Before
	public void setup() throws NoSuchAlgorithmException {
		db = new MockAccountsDb();
		hasher = new MessageDigestPasswordHash(MessageDigest.getInstance("SHA-256"));
	}
	
	@Test
	public void testGetAccountSuccess() throws UnsupportedEncodingException {
		Account account = db.getAccount("john.mcginn@railinc.com", 
				hasher.hashPassword("goats".toCharArray()));
		assertNotNull(account);
	}
}
