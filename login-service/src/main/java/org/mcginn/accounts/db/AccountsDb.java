package org.mcginn.accounts.db;

import org.mcginn.accounts.core.Account;

public interface AccountsDb {

	Account getAccount(String email, byte[] password);
}
