package org.mcginn.accounts.db;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.mcginn.accounts.core.Account;
import org.mcginn.accounts.core.AccountHistory;
import org.mcginn.accounts.login.hash.MessageDigestPasswordHash;
import org.mcginn.accounts.login.hash.PasswordHash;

public class MockAccountsDb implements AccountsDb {

	private final Map<String, Account> accounts;
	private final Map<String, byte[]> credentials;
	
	public MockAccountsDb() throws NoSuchAlgorithmException {
		super();
		
		MessageDigestPasswordHash hasher = new MessageDigestPasswordHash(MessageDigest.getInstance("SHA-256"));
		
		this.accounts = new HashMap<>();
		this.accounts.put("john.mcginn@railinc.com", new Account("john.mcginn@railinc.com", true, false,
				new AccountHistory(LocalDateTime.of(2015, 12, 15, 8, 15, 32), 
				LocalDateTime.of(2015, 12, 21, 11, 30, 25))));
		
		this.credentials = new HashMap<>();
		this.credentials.put("john.mcginn@railinc.com", hash(hasher, "goats"));
	}

	private final byte[] hash(PasswordHash hasher, String pwd) {
		return hasher.hashPassword(pwd.toCharArray());
	}
	
	@Override
	public Account getAccount(String email, byte[] password) {
		Account account = null;
		
		byte[] creds = this.credentials.get(email);
		if(creds != null && password != null && Arrays.equals(password, creds)) {
			account = this.accounts.get(email);
		}
		
		return account;
	}
}
