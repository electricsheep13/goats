package org.mcginn.accounts.login.hash;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;

public class MessageDigestPasswordHash implements PasswordHash {

	private final MessageDigest digest;
	
	public MessageDigestPasswordHash(MessageDigest digest) {
		super();
		this.digest = digest;
	}

	@Override
	public byte[] hashPassword(char[] password) {
		byte[] bytes = null;
		if(password != null) {
			ByteBuffer bb = Charset.forName("UTF-8").encode(CharBuffer.wrap(password));
			bytes = new byte[bb.remaining()];
			bb.get(bytes);

			synchronized (digest) {
				bytes = digest.digest(bytes);
			}
			
			Arrays.fill(bb.array(), (byte)0);
		}

		return bytes;
	}
}
