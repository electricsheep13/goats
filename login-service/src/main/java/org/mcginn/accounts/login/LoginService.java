package org.mcginn.accounts.login;

import javax.servlet.http.HttpServletResponse;

import org.mcginn.accounts.core.Account;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginService {
	
	@RequestMapping(method=RequestMethod.POST, value="/login", produces="application/json")
	public Account login(String email, char[] password, HttpServletResponse response) {
		
		return null;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/login/{sessionId}/{ipAddress}", produces="application/json")
	public Account lookup(@PathVariable("sessionId") String sessionId, 
			@PathVariable("ipAddress") String ipAddress, HttpServletResponse response) {
		return null;
	}
}
