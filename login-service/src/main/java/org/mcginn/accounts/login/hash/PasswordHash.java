package org.mcginn.accounts.login.hash;

public interface PasswordHash {

	byte[] hashPassword(char[] password);
}
