package org.mcginn.owners.listing;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.mcginn.owners.core.Owner;
import org.mcginn.owners.listing.db.OwnersListingDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OwnersListingService {

	@Autowired
	private OwnersListingDb ownersListingDb;
	
	@RequestMapping(value="/owners", method=RequestMethod.GET, produces="application/json")
	public List<Owner> getAllOwners() {
		return this.ownersListingDb.getAllOwners();
	}
	
	@RequestMapping(value="/owners/{name}", method=RequestMethod.GET, produces="application/json")
	public Owner getOwner(@PathVariable("name") String name, HttpServletResponse response) {
		Owner owner = this.ownersListingDb.getOwner(name);
		if(owner == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
		return owner;
	}
}
