package org.mcginn.owners.listing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OwnersListingApp {

	public static void main(String[] args) {
		SpringApplication.run(OwnersListingApp.class, args);
	}
}
