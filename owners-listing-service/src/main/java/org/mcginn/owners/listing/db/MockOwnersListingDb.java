package org.mcginn.owners.listing.db;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.mcginn.owners.core.Owner;
import org.springframework.stereotype.Component;

@Component
public class MockOwnersListingDb implements OwnersListingDb {

	private final Map<String, Owner> ownersByName;
	
	public MockOwnersListingDb() {
		super();
		this.ownersByName = new HashMap<>();
		this.ownersByName.put("McWoukinn Farms", new Owner("McWoukinn Farms"));
		this.ownersByName.put("Frogflat Farms", new Owner("Frogflat Farms"));
	}

	@Override
	public List<Owner> getAllOwners() {
		return filterAndSortOwners(owner -> true);
	}
	
	@Override
	public Owner getOwner(String name) {
		return this.ownersByName.get(name);
	}

	private List<Owner> filterAndSortOwners(Predicate<Owner> filter) {
		return this.ownersByName.values().stream().filter(filter).sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
				.collect(Collectors.toList());
	}
}
