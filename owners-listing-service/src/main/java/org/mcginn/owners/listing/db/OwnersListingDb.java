package org.mcginn.owners.listing.db;

import java.util.List;

import org.mcginn.owners.core.Owner;

public interface OwnersListingDb {

	List<Owner> getAllOwners();
	Owner getOwner(String name);
}
