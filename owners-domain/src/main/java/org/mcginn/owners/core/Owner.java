package org.mcginn.owners.core;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Owner implements Serializable {

	private static final long serialVersionUID = 3783539405429575405L;
	
	private final String name;

	public Owner(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getName()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this) {
			return true;
		}
		
		if(obj == null || !(obj instanceof Owner)) {
			return false;
		}
		
		Owner other = (Owner)obj;
		
		return new EqualsBuilder().append(this.getName(), other.getName()).isEquals();
	}

	@Override
	public String toString() {
		return String.format("%s", getName());
	}
}
