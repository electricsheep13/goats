package org.mcginn.accounts.core;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class AccountHistory implements Serializable {

	private static final long serialVersionUID = -7153988525445797303L;
	
	private final LocalDateTime dateCreated;
	private final LocalDateTime lastLogin;
	
	public AccountHistory(LocalDateTime dateCreated, LocalDateTime lastLogin) {
		super();
		this.dateCreated = dateCreated;
		this.lastLogin = lastLogin;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public LocalDateTime getLastLogin() {
		return lastLogin;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(getDateCreated())
				.append(getLastLogin()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this) {
			return true;
		}
		
		if(obj == null || !(obj instanceof AccountHistory)) {
			return false;
		}
		
		AccountHistory other = (AccountHistory)obj;
		
		return new EqualsBuilder().append(other.getDateCreated(), this.getDateCreated())
				.append(other.getLastLogin(), this.getLastLogin()).isEquals();
	}

	@Override
	public String toString() {
		return String.format("Created on %1$tD %tR and last logged in on %2$tD %tR", getDateCreated(), getLastLogin());
	}
}
