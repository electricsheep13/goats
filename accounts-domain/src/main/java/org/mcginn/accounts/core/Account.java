package org.mcginn.accounts.core;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Account implements Serializable {

	private static final long serialVersionUID = 4473418336395755400L;
	
	private final String email;
	private final boolean active;
	private final boolean locked;
	private final AccountHistory history;
	
	public Account(String email, boolean active, boolean locked, AccountHistory history) {
		super();
		this.email = email;
		this.active = active;
		this.locked = locked;
		this.history = history;
	}

	public String getEmail() {
		return email;
	}

	public boolean isActive() {
		return active;
	}

	public boolean isLocked() {
		return locked;
	}

	public AccountHistory getHistory() {
		return history;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getEmail()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this) {
			return true;
		}
		
		if(obj == null || !(obj instanceof Account)) {
			return false;
		}
		
		Account other = (Account)obj;
		
		return new EqualsBuilder().append(other.getEmail(), this.getEmail()).isEquals();
	}

	@Override
	public String toString() {
		return String.format("%s is active? %b is locked? %b - %s", getEmail(), isActive(), isLocked(), getHistory());
	}
}
